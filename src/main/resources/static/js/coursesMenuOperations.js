var selectedCurr = null;
var modifyCurr = null;
/*
jQuery.validator.addMethod("greaterThan",
    function (value, element, params) {
        if( !/Invalid|NaN/.test(new Date(value)) ) {
            return new Date(value) > new Date( $(params).val() );
        }
            return isNaN(value) && isNaN( $(params).val() )
               || ( Number(value) > Number ($(params).val()) );

    }, 'Must be greater than {0}.');
*/

$('#inputGroupSelectCurr').change(function () {
    selectedCurr = $('#inputGroupSelectCurr option:selected');

    $.when(getCourses( selectedCurr.val())).done();

    //  $('#coursesNameInput').val();
    // alert("dane " + $("select option:selected").val());
});

$('#modifyGroupSelectCurr').change( function () {
    modifyCurr = $('#modifyGroupSelectCurr option:selected');

    console.log(modifyCurr);

    $.when(getCourses2Modify( modifyCurr.val() )).done();
});

var getCourses =  function (code) {
    $.ajax({
        type: 'GET',
        url: '/v1/courses/propositon/' + code +'/',
        dataType: 'json',
        headers: [{'Accept': 'application/json'}],
        success: function (obj, textstatus, xhr) {
            $('#coursesNameInput').val(obj.currency.name);
            $('#buyInput').val(parseFloat(obj.ask));
            $('#sellInput').val(parseFloat(obj.bid));
            //console.log(obj);
            return obj;
        },
        error: function (obj, textstatus) {
            alert( obj.msg)
        }
    });
};
var getCourses2Modify = function(code) {
   $.ajax( {
       type: 'GET',
       url: '/v1/courses/current/' + code +'/',
       dataType: 'json',
       headers: [{'Accept': 'application/json'}],
       success: function (obj, textStatus, xhr) {
           console.log(xhr);
            $('#modBuyInput').val(parseFloat(obj.ask));
            $('#modSellInput').val(parseFloat(obj.bid));
       },
       error: function (obj, textstatus) {
           alert(obj.msg)
       }
   })
};

$('#startDate').datetimepicker({
    format: 'yyyy-mm-ddThh:ii',
    locale: 'pl',
    autoclose: true,
    todayBtn: true,
    minuteStep: 10
});


$('#endDate').datetimepicker({
    format: 'yyyy-mm-ddThh:ii',
    locale: 'pl',
    autoclose: true,
    todayBtn: true,
    minuteStep: 10
});

$('#addCoursesButton').click(
    function () {
        tempObj = new Object();
        tempObj.idCode = $('#inputGroupSelectCurr').val();
        tempObj.ask = $('#buyInput').val();
        tempObj.bid = $('#sellInput').val();
        tempObj.validfrom = $('#startDate').val();
        tempObj.validto = $('#endDate').val();
        if ( tempObj.validto > tempObj.validfrom) {
            $.ajax({
                type: 'POST',
                data: JSON.stringify(tempObj),
                contentType: 'application/json; charset=utf-8',
                url: '/v1/courses/save',
                processData: false,
                success: function (data, textStatus, jQxhr) {
                    alert("saved data for:" + tempObj.idCode);
                    $('#addCourses').modal('toggle');
                    location.reload();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(JSON.stringify(resultJson));
                    console.log(errorThrown);
                }
            });
        }
        else {
            alert("Bad date range !!");
        }
    }
);

$('#delCoursesButton').click(
    function () {
        tempObj = new Object();
        tempObj.idCode = $('#deleteGroupSelectCurr').val();
        if (tempObj.idCode != null) {
            $.ajax( {type:'POST',
                data: JSON.stringify(tempObj),
                contentType : 'application/json; charset=utf-8',
                url: '/v1/courses/delete',
                processData: false,
                success: function (data, textStatus, jQxhr) {
                    alert( JSON.stringify(tempObj));
                    $('#delCourse').modal('toggle');
                    location.reload();
                },
                error: function (jqXrj, textStatus, errorThrown) {
                    console.log(JSON.stringify(tempObj));
                    console.log(errorThrown);
                }
            })
        }
    }
);
