$(document).ready( function () {
    var table = $('#transactionTableList').DataTable({
        "ajax": "/v1/trans/showTransactions",
        "processing": true,
        "paging": true,
        "deferRender": true,
        "pagingType": "simple",
        "ordering": true,
        "lengthMenu": [[10,15, 25, -1], [10,15,25, "All"]],
        "columns": [
            { "data": "id"},
            { "data": "courses.currency.idCode"},
            { "data": "courses.ask" },
            { "data": "courses.bid" },
            { "data": "qty" },
            { "data": "ratio" },
            { "data": "value" },
            { "data": "transactionDate" } ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 2, 3],
                "visible": false
            },
            {
                "targets": [7],
                "render" : function (data, type, row) {
                    var date = new Date(data[0],data[1],data[2],data[3],data[4],data[5] );

                    return date.toLocaleDateString();
                    }
            } ]
        /*"footerCallback" : function (row, data, start, end, display) {
                var api = this.api() , data;
                var intVal = function ( i) {
                    return typeof  i ==='string' ? i.replace(/[\$,]/g,'')*1 : typeof i === 'number' ? i : 0;
                };
                total = api.column(3)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                $(api.column(4).footer ).html('suma: '+ total);
        }*/

    });
  /*  setInterval( function () {
       table.ajax.reload(null, false);
   }, 5000)*/


});

var showBuyModal = function () {
    alert("udało sie ");
};
