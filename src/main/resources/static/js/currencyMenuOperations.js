var myTable = $('#currencyAvailableTable');
var myCurrencyTable = $('#myCurrencyTable');
var selectedRow  = new Array();
var nbpResultTable = new Object();
var resultJson = new Array();
var download = function () {

    var nbpUrl = 'http://api.nbp.pl/api/exchangerates/rates/{table}/';
    var localURL = '/v1/currency/NBPmidtable/';
    var tableTypes = 'A';


    $.ajax({
        type: 'GET',
        url: localURL,
        headers: [{'Accept': 'application/json'}],
       // datatype: 'json',
        success: function (obj, textstatus,xhr) {
            var dateSet = new Array();
                nbpResultTable.tableTypes =obj[0].table;
                nbpResultTable.no = obj[0].no;
                nbpResultTable.effectiveDate = new Date(obj[0].effectiveDate[0],obj[0].effectiveDate[1],obj[0].effectiveDate[2]);
                nbpResultTable.nbpUrl = nbpUrl.replace("/tables/", "/rates/");
                nbpResultTable.active = true;

            dataSet = obj[0].rates;
            myTable = myTable.DataTable({
                data : dataSet,
                deferLoading : 35,
                processing : true,
                pagingType : 'simple',
                select :
                    { style: 'multi',
                    classname: 'selected'},
                language : {
                    select : {
                        rows: {
                            _: "Selected %d rows",
                            0: "Click a row to select it",
                            1: "Selected 1 row"
                        }
                    }
                },
                columnDefs : [
                    {name : "Nazwa" ,        targets : 0},
                    {name : "Kod" ,          targets : 1},
                    {name : "Cena średnia" , targets : 2}
                ],
                columns : [
                    {data: 'currency', name: 'Nazwa'},
                    {data: 'code' , name: 'Kod'},
                    {data: 'mid' , name: 'Cena średnia'}
                ],
                rowCallback : function( row, data, dispalyNum, displayIndex, dataIndex ) {
                    if ( $.inArray(data, selectedRow) !== -1 ) {
                        $(row).addClass('selected');
                    }
                }
            });
        },
        error: function (obj, statustext) {
            alert(obj.msg);
        }
    });
};

var checkAvailable = function() {
    var serviceUrl ='/v1/currency/available';
    $.ajax({
       type: 'GET',
       url: serviceUrl,
       headers: {"Accept" : "application/json"},
       datatype: 'json',
       success: function (obj,statustext) {
           var dateSet = new Array();
            dateSet= obj;
            myCurrencyTable = myCurrencyTable.DataTable({
                data : obj,
                autoFill : true,
                pagingType: 'simple',
                language : {
                    select : {
                        rows: {
                            _: "Selected %d rows",
                            0: "Click a row to select it",
                            1: "Selected 1 row"
                        }
                    }
                },
                select :
                    { style: 'multi',
                        classname: 'selected'},
                columnsDefs: [

                ],
                columns : [
                    {data: 'idCode'},
                    {data: 'name'},
                    {data: 'rates'} ],
                rowCallback : function( row, data, dispalyNum, displayIndex, dataIndex ) {
                    if ( $.inArray(data, selectedRow) !== -1 ) {
                        $(row).addClass('selected');
                    }
                }
            });
       } ,
       error: function (obj,statustext) {
           alert(obj.msg + ' ' + statustext)
       }
    });
}

/*
zaciągnięcie danych z nbp i wypełnienie datatables
 */
$('#addCurrencyMenu').click(function () {
    selectedRow = [];
    download();
});

$('#disCurrencyMenu').click(function () {
    selectedRow = [];
    checkAvailable();
});

$('#disCurrencyButton').click(function () {
    if (resultJson.length > 0) {
        resultJson = [];
    };
    var select2json = myCurrencyTable.rows('.selected').data();
    for (var i = 0 ; i < select2json.length; i++) {
        var tempOjb = new Object();
        tempOjb.idCode = select2json[i].idCode;
        tempOjb.name = select2json[i].name;
        tempOjb.urlNbp = select2json[i].urlNbp;
        tempOjb.tableTypes = select2json[i].tableTypes;
        tempOjb.rates = select2json[i].rates;
        tempOjb.active = false;

        resultJson.push(tempOjb);
    }
    $.ajax({
        type: 'POST',
        data : JSON.stringify(resultJson),
        contentType : 'application/json; charset=utf-8',
        url: '/v1/currency/save',
        success: function (data, textStatus, jQxhr) {
            alert('disabled ' + resultJson.length +' records');
            $('#disableCurrency').modal('toggle');
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(JSON.stringify(resultJson));
            console.log( errorThrown);
        }
    });
    $('#disableCurrency').hide();
    location.reload();
});

/*
zapisanie danych do kolekcji currency
 */
$('#addCurrencyButton').click(function () {

    var selected2Json = myTable.rows('.selected').data();
    for (var i = 0 ; i < selected2Json.length; i++) {
        var tempOjb = new Object();
        tempOjb.idCode = selected2Json[i].code;
        tempOjb.name = selected2Json[i].currency;
        tempOjb.urlNbp = nbpResultTable.nbpUrl;
        tempOjb.tableTypes =nbpResultTable.tableTypes;
        tempOjb.rates = selected2Json[i].mid;
        tempOjb.active = true;

        resultJson.push(tempOjb);
    }

    $.ajax({
        type: 'POST',
        data: JSON.stringify(resultJson),
        // dataType : 'json',
        contentType : 'application/json; charset=utf-8',
        url : '/v1/currency/save',
        processData : false,
        success: function (data, textStatus, jQxhr) {
            alert('success saved ' + resultJson.length + ' records' );
            $('#addCurrency').modal('toggle');
        },
        error : function ( jqXhr, textStatus, errorThrown) {
            console.log(JSON.stringify(resultJson));
            console.log(errorThrown);
        }
    });

    $('#addCurrency').modal('hide');
    location.reload();
});


$('#currencyAvailableTable tbody').on('click', 'tr', function () {
    var id = this.id;
    var index = $.inArray(id, selectedRow);
    if (index === -1)  {
        selectedRow.push(id);
    } else {
        selectedRow.splice(index, 1);
    }
    $(this).toggleClass('selected');
});
$('#myCurrencyTable tbody').on('click','tr',function () {
    var id = this.id;
    var index = $.inArray(id, selectedRow);
    if( index === -1) {
        selectedRow.push(id);
    } else {
        selectedRow.splice(index, 1);
    };
    $(this).toggleClass('selected');
});
