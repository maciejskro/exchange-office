package pl.kayzone.exchangeservice;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersistenceConfiguration {

    public @Bean
    MongoClient mongoClient() {
        return new MongoClient("localhost");
    }

    public String getDatabaseName() {
        return "exchange";
    }
}
