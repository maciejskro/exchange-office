package pl.kayzone.exchangeservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.kayzone.exchangeservice.entity.Currency;
import pl.kayzone.exchangeservice.entity.CurrencyCourses;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CurrencyCourseRepository extends MongoRepository<CurrencyCourses, ObjectId> {

    List<CurrencyCourses> findByCurrencyIs(Currency c);
    List<CurrencyCourses> findByCurrency_IdCode(String idCode);

    List<CurrencyCourses> findAllByValidFromIsAfterAndValidToBefore(LocalDateTime start, LocalDateTime end);
    List<CurrencyCourses> findAllByValidFromBeforeAndValidToAfter(LocalDateTime start, LocalDateTime end);

    List<CurrencyCourses> findByCurrency_IdCodeAndValidFromIsBeforeAndValidToIsBefore(String idCode, LocalDateTime start, LocalDateTime end);
    List<CurrencyCourses> findByCurrency_IdCodeAndValidFromIsBeforeAndValidToIsAfter(String idCode, LocalDateTime start, LocalDateTime end);
    List<CurrencyCourses> findByCurrency_IdCodeAndValidFromIsAfterAndValidToIsBefore(String idCode, LocalDateTime start, LocalDateTime end);
    List<CurrencyCourses> findByCurrency_IdCodeAndValidFromIsAfterAndValidToIsAfter(String idCode, LocalDateTime start, LocalDateTime end);

    List<CurrencyCourses> findByCurrency_IdCodeAndValidFromIsOrValidToIs(String idCode, LocalDateTime start, LocalDateTime end);

}
