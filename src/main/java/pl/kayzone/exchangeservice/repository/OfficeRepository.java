package pl.kayzone.exchangeservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import pl.kayzone.exchangeservice.entity.Office;

import java.util.List;

@Repository
public interface OfficeRepository extends MongoRepository<Office, ObjectId> {

    List<Office> findByPartnerName(String name);

    @Query("{office.shortname : ?0}")
    List<Office> findByOfficeListContains(String locationName);

}
