package pl.kayzone.exchangeservice.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.kayzone.exchangeservice.entity.NbpCurrencyTable;
import pl.kayzone.exchangeservice.entity.NbpSingleCurrency;

import java.util.*;

@Repository
public class NbpApiRepository {

    @Autowired
    private RestTemplate restTemplate;

    private Map<String, String> paramMap;

    private String currentNBPurl;

    public NbpApiRepository() {
        this.paramMap = new HashMap<>();
    }

    private String nbpUrl = "http://api.nbp.pl/api/exchangerates/";

    public List<NbpCurrencyTable> getAvailableCurrency(String table) {
        String nbpurl = nbpUrl + "tables/{table}/";
        currentNBPurl = nbpurl;
        NbpCurrencyTable nbpresult[] = null;
        paramMap.put("table", table);
        try {
            nbpresult = restTemplate.getForObject(nbpurl, NbpCurrencyTable[].class, paramMap);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        }
        return new ArrayList<>(Arrays.asList(nbpresult));
    }

    public Optional<NbpSingleCurrency> getCurrentRate4Currency(String table , String idCode)  {
        String nbpurl = nbpUrl + "rates/{table}/{code}";
        currentNBPurl = nbpurl;
        ResponseEntity<NbpSingleCurrency> nbpresult = null;
        paramMap.put("table", table);
        paramMap.put("code", idCode);
        try{
            nbpresult = restTemplate.getForEntity(nbpurl, NbpSingleCurrency.class, paramMap);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                return Optional.empty();
            } else
                throw e;
        }
        return Optional.of(nbpresult.getBody());
    }

    public String getCurrentNBPurl() {
        return currentNBPurl;
    }

    public void setCurrentNBPurl(String currentNBPurl) {
        this.currentNBPurl = currentNBPurl;
    }
}
