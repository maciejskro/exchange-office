package pl.kayzone.exchangeservice.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.kayzone.exchangeservice.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User, ObjectId> {

    List<User> findUserByUsername(String username);



}
