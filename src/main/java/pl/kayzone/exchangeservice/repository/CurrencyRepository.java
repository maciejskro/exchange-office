package pl.kayzone.exchangeservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.kayzone.exchangeservice.entity.Currency;

import java.util.List;

@Repository
public interface CurrencyRepository extends MongoRepository<Currency, String> {

    public Currency findByIdCode(String id);

    public List<Currency> findAllByActiveIsTrue();

}
