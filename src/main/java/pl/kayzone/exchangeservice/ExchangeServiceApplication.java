package pl.kayzone.exchangeservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.kayzone.exchangeservice.entity.User;
import pl.kayzone.exchangeservice.repository.CurrencyCourseRepository;
import pl.kayzone.exchangeservice.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.function.Supplier;

@SpringBootApplication
public class ExchangeServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run( ExchangeServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(UserRepository userRepository, PasswordEncoder passwordEncoder ,CurrencyCourseRepository ccr) {
        return args -> {
            User user = User.builder()
                    .fistname("admin")
                    .lastname("admin")
                    .username("admin")
                    .userRole("USER")
                    .email("admin@admin.pl")
                    .pass( passwordEncoder.encode("admin123") )
                    .build();
            if (userRepository.findUserByUsername(user.getUsername()).isEmpty()  ) {
                userRepository.save(user);
            }
            System.out.println("CommandLineRunner running in the Application class...");

          /*  CurrencyCourses cc = CurrencyCourses.builder()
                    .ask(new BigDecimal(3.445))
                    .bid(new BigDecimal(4.566))
                    .currency(new Currency("USD","dolar","https://","A", 1.0, LocalDateTime.now()))
                    .validFrom(LocalDateTime.of(LocalDate.now().minusDays(1l) , LocalTime.now().minusHours(12l)) )
                    .validTo(LocalDateTime.of(LocalDate.now().plusDays(1l),LocalTime.now().plusHours(12l) ))
                    .build();
            if (ccr.findAll() == null) {
                ccr.save(cc);
            }*/

        };
    }

    @Bean
    public Supplier<LocalDateTime> localDateTimeSupplier(){
        return LocalDateTime::now;
    }
}
