package pl.kayzone.exchangeservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kayzone.exchangeservice.entity.Currency;
import pl.kayzone.exchangeservice.services.CurrencyServices;
import pl.kayzone.exchangeservice.services.ICurrencyServices;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CurrencyController {

    @Autowired
    CurrencyServices services;

    @PostMapping("/currency/create")
    public String saveCurrency(@ModelAttribute Currency c) {
        services.saveCurr(c);
        return "redirect:/transaction.html";
    }

    @GetMapping("/currency/setform")
    public String getCurrencyFromNBP(Model m) {
        /*
        * wywołuje serwis pobierający listę kursów z nbp i wstawia do ziennej listCurrency którą wyślemy do formularza*/
        List<Currency> lista = null;
        m.addAttribute("listCurrency", lista );
        return "fragments/modalDialog.html :: modalAddCurrency";
    }


    public List<Currency> getCurrencyList() {
        List<Currency> result = new ArrayList<>();
        return result;
    }

}
