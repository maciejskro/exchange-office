package pl.kayzone.exchangeservice.controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import pl.kayzone.exchangeservice.entity.CurrencyCourses;
import pl.kayzone.exchangeservice.entity.dto.AjaxTransaction;
import pl.kayzone.exchangeservice.services.CurrencyCourseServices;
import pl.kayzone.exchangeservice.services.CurrencyServices;
import pl.kayzone.exchangeservice.services.TransactionService;

@Controller
public class CurrencyCoursesController {

    @Autowired
    private CurrencyCourseServices currCours;

    @Autowired
    private CurrencyServices currencyServices;

    @Autowired
    private TransactionService transactionService;

    @GetMapping("/")
    public String getAllAcutalCurrency(Model m) {
        List<CurrencyCourses> currencyCoursesList = currCours.findAllByValidNow();
        AjaxTransaction trans = transactionService.findByTransactionDateBetween(LocalDateTime.now());
        m.addAttribute("currencyList", currencyCoursesList);
        m.addAttribute("availableCurrency", currencyServices.getAllActualCurrency() );
        return "transactions";
    }


}
