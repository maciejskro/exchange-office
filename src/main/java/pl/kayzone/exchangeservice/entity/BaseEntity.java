package pl.kayzone.exchangeservice.entity;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document
@Getter
@Setter
public abstract class BaseEntity implements Serializable {

    @Id
    private ObjectId id;
    @Version
    private Long version;
}
