package pl.kayzone.exchangeservice.entity;

import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Document
@Builder
@TypeAlias("transaction")
public class User extends BaseEntity{

    @Indexed
    private String username;
    private String fistname;
    private String lastname;
    private String userRole;
    private String email;
    private String pass;
    @DBRef
    private OfficeLocation userOffice;
}
