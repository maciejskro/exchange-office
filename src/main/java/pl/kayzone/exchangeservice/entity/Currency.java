package pl.kayzone.exchangeservice.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Document()
@Builder
@TypeAlias("currency")
public class Currency implements Serializable {

    @Id
    private String idCode;
    private String name;
    @Field(value = "urlNBP")
    private String urlNbp;
    @Field(value = "table")
    private String tableTypes;
    private BigDecimal rates;
    private Boolean active;
    private LocalDateTime lastUpdate;

}