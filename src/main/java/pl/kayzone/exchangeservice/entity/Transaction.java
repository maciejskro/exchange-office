package pl.kayzone.exchangeservice.entity;

import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
@Document()
@TypeAlias("transaction")
public class Transaction extends BaseEntity{

    @DBRef
    private CurrencyCourses courses;
    private BigDecimal qty;
    private BigDecimal ratio;
    private BigDecimal value;
    private LocalDateTime transactionDate;
}
