package pl.kayzone.exchangeservice.entity.dto;

import pl.kayzone.exchangeservice.entity.Transaction;

import java.io.Serializable;
import java.util.List;

public class AjaxTransaction implements Serializable {
    List<Transaction>  data;

    public AjaxTransaction(List<Transaction> data) {
        this.data = data;
    }

    public List<Transaction> getData() {
        return data;
    }

    public void setData(List<Transaction> data) {
        this.data = data;
    }
}
