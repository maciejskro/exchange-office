package pl.kayzone.exchangeservice.entity.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class HelperCourses {

    @JsonProperty("idCode")
    private String idCode;

    @JsonProperty("ask")
    private BigDecimal askVal;

    @JsonProperty("bid")
    private BigDecimal bidVal;

    @JsonProperty("validfrom")
    private LocalDateTime validfrom;

    @JsonProperty("validto")
    private LocalDateTime validto;

    @JsonProperty("idCode")
    public String getIdCode() {
        return idCode;
    }
    @JsonProperty("idCode")
    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    @JsonProperty("ask")
    public BigDecimal getAskVal() {
        return askVal;
    }
    @JsonProperty("ask")
    public void setAskVal(BigDecimal askVal) {
        this.askVal = askVal;
    }

    @JsonProperty("bid")
    public BigDecimal getBidVal() {
        return bidVal;
    }

    @JsonProperty("bid")
    public void setBidVal(BigDecimal bidVal) {
        this.bidVal = bidVal;
    }

    @JsonProperty("validfrom")
    public LocalDateTime getValidfrom() {
        return validfrom;
    }

    @JsonProperty("validfrom")
    public void setValidfrom(LocalDateTime validfrom) {
        this.validfrom = validfrom;
    }

    @JsonProperty("validto")
    public LocalDateTime getValidto() {
        return validto;
    }

    @JsonProperty("validto")
    public void setValidto(LocalDateTime validto) {
        this.validto = validto;
    }
}
