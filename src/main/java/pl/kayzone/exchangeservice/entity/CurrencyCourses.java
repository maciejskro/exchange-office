package pl.kayzone.exchangeservice.entity;

import lombok.*;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Document
@Builder
@TypeAlias("curr_courses")
public class CurrencyCourses extends BaseEntity{

    @DBRef
    private Currency currency;
    @DBRef
    private OfficeLocation officeLocation;
    private BigDecimal ask;
    private BigDecimal bid;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;

}
