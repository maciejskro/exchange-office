package pl.kayzone.exchangeservice.entity;

import com.fasterxml.jackson.annotation.*;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonPropertyOrder({
        "table",
        "no",
        "effectiveDate",
        "rates"
})
public class NbpCurrencyTable {

    @JsonProperty("table")
    private String table;
    @JsonProperty("no")
    private String no;
    @JsonProperty("tradingDate") @Nullable
    private LocalDate tradingDate;
    @JsonProperty("effectiveDate")
    private LocalDate effectiveDate;
    @JsonProperty("rates")
    private List<Rates> rates = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("table")
    public String getTable() {
        return table;
    }

    @JsonProperty("table")
    public void setTable(String table) {
        this.table = table;
    }

    @JsonProperty("no")
    public String getNo() {
        return no;
    }

    @JsonProperty("no")
    public void setNo(String no) {
        this.no = no;
    }

    @JsonProperty("tradingDate")
    public LocalDate getTradingDate() {
        return tradingDate;
    }

    @JsonProperty("tradingDate")
    public void setTradingDate(LocalDate tradingDate) {
        this.tradingDate = tradingDate;
    }

    @JsonProperty("effectiveDate")
    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    @JsonProperty("effectiveDate")
    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @JsonProperty("rates")
    public List<Rates> getRates() {
        return rates;
    }

    @JsonProperty("rates")
    public void setRates(List<Rates> rates) {
        this.rates = rates;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
