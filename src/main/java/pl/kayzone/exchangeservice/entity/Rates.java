package pl.kayzone.exchangeservice.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.Nullable;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Rates {

    @JsonProperty("currency")
    private String currency;
    @JsonProperty("code")
    private String code;
    @JsonProperty("mid") @Nullable
    private BigDecimal mid;
    @JsonProperty("bid") @Nullable
    private BigDecimal bid;
    @JsonProperty("ask") @Nullable
    private BigDecimal ask;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("mid") @Nullable
    public BigDecimal getMid() {
        return mid;
    }

    @JsonProperty("mid") @Nullable
    public void setMid(BigDecimal mid) {
        this.mid = mid;
    }

    @JsonProperty("bid") @Nullable
    public BigDecimal getBid() {
        return bid;
    }

    @JsonProperty("bid") @Nullable
    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    @JsonProperty("ask") @Nullable
    public BigDecimal getAsk() {
        return ask;
    }

    @JsonProperty("ask") @Nullable
    public void setAsk(BigDecimal ask) {
        this.ask = ask;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
