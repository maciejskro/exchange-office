package pl.kayzone.exchangeservice.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.Nullable;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class SingleRates {

    @JsonProperty("no")
    private String no;
    @JsonProperty("effectiveDate")
    private LocalDate effectiveDate;
    @JsonProperty("mid") @Nullable
    private BigDecimal mid;
    @JsonProperty("bid") @Nullable
    private BigDecimal bid;
    @JsonProperty("ask") @Nullable
    private BigDecimal ask;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("no")
    public String getNo() {
        return no;
    }

    @JsonProperty("no")
    public void setNo(String no) {
        this.no = no;
    }

    @JsonProperty("effectiveDate")
    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    @JsonProperty("effectiveDate")
    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @JsonProperty("mid") @Nullable
    public BigDecimal getMid() {
        return mid;
    }

    @JsonProperty("mid") @Nullable
    public void setMid(BigDecimal mid) {
        this.mid = mid;
    }

    @JsonProperty("bid") @Nullable
    public BigDecimal getBid() {
        return bid;
    }

    @JsonProperty("bid") @Nullable
    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    @JsonProperty("ask") @Nullable
    public BigDecimal getAsk() {
        return ask;
    }

    @JsonProperty("ask") @Nullable
    public void setAsk(BigDecimal ask) {
        this.ask = ask;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
