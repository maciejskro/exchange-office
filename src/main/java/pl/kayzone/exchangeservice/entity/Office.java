package pl.kayzone.exchangeservice.entity;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@TypeAlias("office")

public class Office   {
    @Id
    private ObjectId id;
    private String partnerName;
    private String partnerNo;
    private List<OfficeLocation> officeList;
}
