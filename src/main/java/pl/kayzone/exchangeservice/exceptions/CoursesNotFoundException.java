package pl.kayzone.exchangeservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class CoursesNotFoundException extends Exception {

    public CoursesNotFoundException(String message) {
        System.out.println(message);
    }
}
