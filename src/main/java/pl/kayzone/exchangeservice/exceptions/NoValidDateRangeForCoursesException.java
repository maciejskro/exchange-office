package pl.kayzone.exchangeservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "No valid date range was set for courses")
public class NoValidDateRangeForCoursesException extends RuntimeException {

    private String msg;
    public NoValidDateRangeForCoursesException(String message) {
            this.msg = message;
    }
}
