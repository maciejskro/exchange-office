package pl.kayzone.exchangeservice.exceptions;

public class HistoricalRangeOverrideException extends RuntimeException
{
   private String message;
   public HistoricalRangeOverrideException(String message)
   {
      super(message);
      this.message = message;
   }
}
