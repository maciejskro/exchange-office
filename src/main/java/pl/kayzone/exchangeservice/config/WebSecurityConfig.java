package pl.kayzone.exchangeservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import pl.kayzone.exchangeservice.services.UserSecurityService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserSecurityService userSecurityService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/js/**").authenticated()
                .antMatchers("/fragments/**").authenticated()
                .antMatchers("/layouts/**").authenticated()
                .antMatchers("/v1/trans/**").permitAll()
                .antMatchers("/v1/currency/**").permitAll()
                .antMatchers("/v1/courses/**").permitAll()
                .antMatchers("/").authenticated()
                .antMatchers("/user/**").hasAnyRole("ADMIN" )
                .antMatchers("/currency/**").hasAnyRole("ADMIN")
            //    .antMatchers("/transaction").authenticated()
                .antMatchers("/books/all").hasAnyRole("USER","ADMIN")
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/login").permitAll()
                .and()
                .logout().logoutSuccessUrl("/login").permitAll()
                .disable();
        http.csrf().disable();
    }

    @Bean
    public  PasswordEncoder passwordEncoder() {
        return  new BCryptPasswordEncoder();
    }

    @Bean
    public HttpFirewall allowUrlEndoceSlashHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedSlash(true);
        return firewall;
    }

    @Override
    public void configure(WebSecurity web) throws  Exception  {
        super.configure(web);
        web.httpFirewall(allowUrlEndoceSlashHttpFirewall());
    }

/*
      @Bean
      @Override public UserDetailsService userDetailsService() { return new UserDetailsService(){
      @Override public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
      return new UserDetails() {
      @Override public Collection<? extends GrantedAuthority> getAuthorities() { return
      Arrays.asList(new SimpleGrantedAuthority("USER")); }
      @Override public String getPassword() { return "root"; }
      @Override public String getUsername() { return "root"; }
      @Override public boolean isAccountNonExpired() { return true; }
      @Override public boolean isAccountNonLocked() { return true; }
      @Override public boolean isCredentialsNonExpired() { return true; }
      @Override public boolean isEnabled() { return true; } }; } }; }
      @Bean public NoOpPasswordEncoder passwordEncoder() { return (NoOpPasswordEncoder)
      NoOpPasswordEncoder.getInstance(); }
*/

/*
    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        UserDetails admin = User.withDefaultPasswordEncoder()
                .username("admin")
                .password("admin")
                .roles("ADMIN", "USER")
                .build();

        InMemoryUserDetailsManager inMemoryUserDetailsManager = new InMemoryUserDetailsManager(admin);

        UserDetails user = User.withDefaultPasswordEncoder()
                .username("user")
                .password("user")
                .roles("USER")
                .build();

        inMemoryUserDetailsManager.createUser(user);

        return inMemoryUserDetailsManager;
    }
*/

    protected void configure(AuthenticationManagerBuilder authManBuild) {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userSecurityService);
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        authManBuild.authenticationProvider(daoAuthenticationProvider);
    }

}
