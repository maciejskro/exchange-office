package pl.kayzone.exchangeservice.config;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoClientFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoTypeMapper;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "pl.kayzone.exchangeservice")
public class PersistenceConfiguration extends AbstractMongoConfiguration {

    public @Bean
    MongoClientFactoryBean mongoClientFactoryBean() {
        MongoClientFactoryBean mcfb = new MongoClientFactoryBean();
        mcfb.setHost("localhost");
        return mcfb;
    }

    @Override
    public MongoClient mongoClient() {
        return new MongoClient("localhost");
    }

    @Bean
    public  MongoDbFactory mongoDbFactory()  {
        SimpleMongoDbFactory result = null;
        try {
            result = new SimpleMongoDbFactory(mongoClient(), getDatabaseName() );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected String getDatabaseName() {
        return "exchange";
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        return  new MongoTemplate(mongoClient(), getDatabaseName());
    }

    @Bean
    public MappingMongoConverter mappingMongoConverter () throws  Exception {
        MappingMongoConverter mmc = super.mappingMongoConverter();
        mmc.setTypeMapper(customTypeMapper());
        return mmc;
    }

    @Bean
    public MongoTypeMapper customTypeMapper() {
        //return  new CustomMongoTypeMapper() ;
        return null;
    }

}
