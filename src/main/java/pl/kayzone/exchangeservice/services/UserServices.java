package pl.kayzone.exchangeservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import pl.kayzone.exchangeservice.entity.User;
import pl.kayzone.exchangeservice.repository.UserRepository;

@Service
public class UserServices {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/user/create")
    public void createUser(User u) {
        userRepository.save(u);
    }
}
