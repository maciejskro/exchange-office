package pl.kayzone.exchangeservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.kayzone.exchangeservice.entity.User;
import pl.kayzone.exchangeservice.entity.wrappers.UserPrincipalDetail;
import pl.kayzone.exchangeservice.repository.UserRepository;

import java.util.List;

@Service
public class UserSecurityService implements UserDetailsService {


    @Autowired
    private UserRepository userRepository;

    public UserDetails loadUserByUsername(String username) {
        List<User> u = userRepository.findUserByUsername(username);
        if (u.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        return new UserPrincipalDetail(u.get(0));
    }
}
