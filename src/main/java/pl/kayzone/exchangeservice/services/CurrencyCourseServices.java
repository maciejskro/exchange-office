package pl.kayzone.exchangeservice.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.kayzone.exchangeservice.entity.Currency;
import pl.kayzone.exchangeservice.entity.CurrencyCourses;
import pl.kayzone.exchangeservice.entity.NbpSingleCurrency;
import pl.kayzone.exchangeservice.exceptions.CoursesNotFoundException;
import pl.kayzone.exchangeservice.exceptions.HistoricalRangeOverrideException;
import pl.kayzone.exchangeservice.exceptions.NoValidDateRangeForCoursesException;
import pl.kayzone.exchangeservice.repository.CurrencyCourseRepository;
import pl.kayzone.exchangeservice.repository.NbpApiRepository;

@Service
@Transactional
public class CurrencyCourseServices {

    private static final Logger LOGGER = Logger.getLogger(CurrencyCourseServices.class.getName());

    private CurrencyCourseRepository currCourRepo;

    private CurrencyServices cs;

    private NbpApiRepository nbpRepo;

    private HashMap<String, CurrencyCourses> cachedList;

    private ObjectMapper mapper;

    public CurrencyCourseServices() {
        cachedList = new HashMap<>();
        mapper = new ObjectMapper();
    }

    @Autowired
    public void setCurrCourRepo(CurrencyCourseRepository currCourRepo) {
        this.currCourRepo = currCourRepo;
    }

    @Autowired
    public void setCs(CurrencyServices cs)  {
        this.cs = cs;
    }

    @Autowired
    public void setNbpRepo(NbpApiRepository nbpRepo)  {
        this.nbpRepo = nbpRepo;
    }

    public List<CurrencyCourses> findAllByValidNow(LocalDateTime st, LocalDateTime en) {
        return currCourRepo.findAllByValidFromBeforeAndValidToAfter(st, en);
    }

    public List<CurrencyCourses> findAllByValidNow() {
        LocalDateTime current = LocalDateTime.now();
        return findAllByValidNow (current,current);
    }

    private List<CurrencyCourses> findValidCurrentCode(String code) {
        List<CurrencyCourses> templist = findAllByValidNow();
        return templist.stream().filter(currencyCourses -> currencyCourses.getCurrency().getIdCode().equals(code) )
              .collect(Collectors.toList());
    }

    public CurrencyCourses findProposition(String idCode) {
        LocalDateTime tera = LocalDateTime.now();
        Optional<NbpSingleCurrency> nbpSingle;
        List<Currency> listCC = cs.getAllActualCurrency();
        for (Currency c : listCC) {
                nbpSingle = nbpRepo.getCurrentRate4Currency("C",c.getIdCode());
                CurrencyCourses cc = new CurrencyCourses();
                    cc.setCurrency(c);
                    if (nbpSingle.isPresent()) {
                        cc.setAsk(nbpSingle.get().getRates().get(0).getAsk());
                        cc.setBid(nbpSingle.get().getRates().get(0).getBid());
                    } else
                    {
                        cc.setAsk(BigDecimal.ZERO);
                        cc.setBid(BigDecimal.ZERO);
                    }
                    cc.setValidFrom(tera);
                    cc.setValidTo(tera.plusDays(1L));
                cachedList.put(c.getIdCode(),cc);
        }
        return  cachedList.get(idCode);
    }

    public void saveCurrencyCourse(String strtosave) throws CoursesNotFoundException {
        CurrencyCourses cc = new CurrencyCourses();
        List<Currency> availableC = cs.getAllActualCurrency();  // get currency with active = true
        List<CurrencyCourses> currentCours = null;
        try {
            JsonNode rootNode = mapper.readTree(strtosave);
            // check if is equals new id code and its in database
            String code = rootNode.get("idCode").asText();
            for (Currency c : availableC) {
                if (c.getIdCode().equals(code)) {
                    cc.setCurrency(c);
                    currentCours = findValidCurrentCode(code);  // sprawdzam czy istnieje aktualny kurs dla waluty
                } else if ( code.isEmpty() || code.length() == 0 ) {
                    throw new CoursesNotFoundException("Courses not found in active for market");
                    //TODO tu coś wymyślić - nie ma aktywnych walut do zpisu kursu.
                }
            }
            // check valid date
            cc.setBid(new BigDecimal(rootNode.get("bid").asText()));
            cc.setAsk(new BigDecimal(rootNode.get("ask").asText()));
            cc.setValidFrom(LocalDateTime.parse(rootNode.get("validfrom").asText()));
            cc.setValidTo(LocalDateTime.parse(rootNode.get("validto").asText()));

            String pointer = getValidRange(cc);
            LOGGER.log(Level.ALL,"pointer: %s",  pointer );
            // cc variable is to save
            switch (pointer) {
                case "notexists":
                    // w tym przedziale czasowym nie stnieje żaden aktualny kurs dla tej waluty
                    currCourRepo.save(cc);
                    break;
                case "bad range":
                    throw  new NoValidDateRangeForCoursesException("Not valid date for new Currency");
                case "historical_range":
                    throw new HistoricalRangeOverrideException("Historical range override is not allowed");
                case "before":
                    // throw not allowed because
                    updateDate(cachedList);
                    currCourRepo.save(cc);
                    throw new NoValidDateRangeForCoursesException("whole date argument is befor!");
                case "inside":
                    updateDate(cachedList);
                    currCourRepo.save(cc);
                    throw new NoValidDateRangeForCoursesException("whole date argument is inside!");
                case "after":
                    // save with correct other courses ???
                    updateDate(cachedList);
                    currCourRepo.save(cc);
                case "over":
                    updateDate(cachedList);
                    currCourRepo.save(cc);
                    throw new NoValidDateRangeForCoursesException("whole date argument is over!");
                default:
                    throw new IllegalArgumentException(" save not allowed - bad arguments");
            }

            LOGGER.log(Level.ALL, strtosave );
            LOGGER.log(Level.ALL, currentCours!=null? currentCours.toString():"");

        } catch (IOException e) {
            e.printStackTrace();
            //TODO zwraca alert do web że nie może zapisać
        }
    }

    private String getValidRange ( CurrencyCourses newCourseToSave) {
        String code = newCourseToSave.getCurrency().getIdCode();
        LocalDateTime start = newCourseToSave.getValidFrom();
        LocalDateTime end = newCourseToSave.getValidTo();
        LocalDateTime now = LocalDateTime.now();
        String pointer = null;
        if ( ! cachedList.isEmpty() ) {
            cachedList.clear();
        }
        if (end.isBefore(start)) {
            return "bad_range";
        }
        if (end.isBefore(now)) {
            return "historical_range";
        }

        List<CurrencyCourses> savedInDb = currCourRepo.findByCurrency_IdCodeAndValidFromIsAfterAndValidToIsBefore(code, start, end); // wewnątrz zakresu do zapisu
            savedInDb.addAll( currCourRepo.findByCurrency_IdCodeAndValidFromIsBeforeAndValidToIsAfter(code , start, start) ); // przecina początek zakresu do zapisu
            savedInDb.addAll( currCourRepo.findByCurrency_IdCodeAndValidFromIsBeforeAndValidToIsAfter(code, end, end) ); // przecina koniec zakresu do zapisu
            savedInDb.addAll( currCourRepo.findByCurrency_IdCodeAndValidFromIsBeforeAndValidToIsAfter( code, start, end )); // rozciąga się nad całym zakresem do zapisu
            savedInDb.addAll( currCourRepo.findByCurrency_IdCodeAndValidFromIsOrValidToIs(code, start, end)); // taki sma zakres jak ten do zapisu

        if ( savedInDb.isEmpty() ) {
            return "notexists";
            // nie istneje kurs z IdCode w przedziale czaswym start end
        }
        // savedInDb - na liśie są zapisy kolizyjne, kolidują z istniejącymi bazami.
        for (CurrencyCourses currC : savedInDb)  {
            if (currC.getValidTo().isEqual(start) && currC.getValidTo().isEqual(end) ) {
                cachedList.put("toUpdateCourses:" + currC.hashCode(), currC);
                pointer = "toUpdate";
            } else  if ( currC.getValidFrom().isBefore(start) &&
                    currC.getValidTo().isBefore(end) && currC.getValidTo().isAfter(start)) {
                currC.setValidTo(start);
                cachedList.put("toUpdateDate:" + currC.hashCode(), currC);
                pointer = "before";
            } else if ( currC.getValidFrom().isAfter(start) && currC.getValidFrom().isBefore(start) &&
                    currC.getValidTo().isAfter(end)) {
                currC.setValidFrom(end);
                cachedList.put("toUpdateDate:"+currC.hashCode(), currC);
                pointer = "after";
            } else if ( currC.getValidFrom().isBefore(start) && currC.getValidTo().isAfter(end)) {
                currC.setValidTo(start);
                cachedList.put("toUpdateDate:"+currC.hashCode(), currC);
                pointer = "toUpdate";
            }
        }
        return pointer;
    }

    private LocalDateTime getLastValidDate(List<CurrencyCourses> lista) {
        LocalDateTime result;
        if (lista != null || ! lista.isEmpty()) {
            result = lista.get(0).getValidTo();
        } else {
            result = LocalDateTime.now();
            return null;
        }
        for (CurrencyCourses c: lista) {
            if (c.getValidTo().isAfter(result)) {
                result = c.getValidTo();
            }
        }
        return result;
    }
    private void updateDate(HashMap<String, CurrencyCourses> toUpdate) {
        toUpdate.forEach( (k,v) -> {
            currCourRepo.save(v);
        } );
    }

    public void deleteCurrencyCourse(String deleteCourse)
    {
        try {
            JsonNode rootNode = mapper.readTree(deleteCourse);
            String code = rootNode.get("idCode").asText();
            List<CurrencyCourses> ccList = findValidCurrentCode(code);
            for (CurrencyCourses c : ccList) {
                c.setValidTo(LocalDateTime.now());
                currCourRepo.save(c);
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
