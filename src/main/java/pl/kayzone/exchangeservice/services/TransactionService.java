package pl.kayzone.exchangeservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kayzone.exchangeservice.entity.Transaction;
import pl.kayzone.exchangeservice.entity.dto.AjaxTransaction;
import pl.kayzone.exchangeservice.repository.TransactionRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.List;


@Service
public class TransactionService {

    @Autowired
    private TransactionRepository repository;


    public AjaxTransaction findByTransactionDateBetween (LocalDateTime now) {
        LocalDateTime start = now.withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
        LocalDateTime end = now.with(TemporalAdjusters.lastDayOfMonth());
        List<Transaction> listaTrans = repository.findByTransactionDateBetween(start, end);
        AjaxTransaction at = new AjaxTransaction(listaTrans);
        return  at;
    }

    public void sell ( Transaction t) {
        /*
        * przy sprzedaży mamy ratio ujemne i value ujemne
        * */
        if (t.getRatio().compareTo(BigDecimal.ZERO) <0 ) {
            t.setRatio(t.getRatio().negate());
        }
        if (t.getValue().compareTo(BigDecimal.ZERO) <0 ) {
            t.setValue(t.getValue().negate());
        }
        if( t.getRatio().compareTo(BigDecimal.ZERO) == 0 | t.getValue().compareTo(BigDecimal.ZERO) ==0) {
            return ;
        } else {
            repository.save(t);
        }
    }

    public void buy( Transaction t) {
        /*
        * przy zakupie mamy radio dodatnie i value dodatanie
        * */
        if (t.getRatio().compareTo(BigDecimal.ZERO) >0 ) {
            t.setRatio(t.getRatio().negate());
        }
        if (t.getValue().compareTo(BigDecimal.ZERO) >0 ) {
            t.setValue(t.getValue().negate());
        }
        if( t.getRatio().compareTo(BigDecimal.ZERO) == 0 | t.getValue().compareTo(BigDecimal.ZERO) ==0) {
            return ;
        } else {
            repository.save(t);
        }
    }
}
