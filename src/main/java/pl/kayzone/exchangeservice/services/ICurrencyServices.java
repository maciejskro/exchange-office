package pl.kayzone.exchangeservice.services;

import org.springframework.stereotype.Service;
import pl.kayzone.exchangeservice.entity.Currency;

import java.util.List;

@Service
public interface ICurrencyServices {

    public void saveCurr(Currency c);
    public List<Currency> getAllActualCurrency();
}
