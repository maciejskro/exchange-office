package pl.kayzone.exchangeservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kayzone.exchangeservice.entity.Currency;
import pl.kayzone.exchangeservice.entity.NbpCurrencyTable;
import pl.kayzone.exchangeservice.repository.CurrencyRepository;
import pl.kayzone.exchangeservice.repository.NbpApiRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CurrencyServices  implements ICurrencyServices {

    private CurrencyRepository currencyRepository;

    private NbpApiRepository nbpApiRepository;

    @Autowired
    public void setCurrencyRepository(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Autowired
    public void setNbpApiRepository(NbpApiRepository nbpApiRepository)  {
        this.nbpApiRepository = nbpApiRepository;
    }

    public void saveCurr(Currency c) {
        Currency existing = doesExistCode(c);
        if ( c != null &&  existing==null  ) {
            if (c.getLastUpdate() != null  ) {
                currencyRepository.save(c);
            } else
                c.setLastUpdate(LocalDateTime.now());
                currencyRepository.save(c);
        } else {
            existing.setLastUpdate(LocalDateTime.now());
            existing.setRates(c.getRates());
            existing.setName(c.getName());
            existing.setActive(c.getActive());
            existing.setUrlNbp(c.getUrlNbp());
            currencyRepository.save(existing);
        }
    }

    public List<Currency> getAllActualCurrency() {
        return currencyRepository.findAllByActiveIsTrue();
    }

    public List<NbpCurrencyTable> getNbpCurrencyMidTable() {
        return nbpApiRepository.getAvailableCurrency("A");
    }

    private Currency doesExistCode(Currency c) {
        return currencyRepository.findByIdCode(c.getIdCode());
    }

}
