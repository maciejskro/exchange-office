package pl.kayzone.exchangeservice.converters;

import org.springframework.format.datetime.DateFormatter;
import pl.kayzone.exchangeservice.entity.Transaction;
import pl.kayzone.exchangeservice.entity.dto.AjaxTransaction;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class AjaxTransactionConverter {


    public AjaxTransaction convert(List<Transaction> input) {
        List<List<String>> toResult = new ArrayList<>();
        for (Transaction t : input) {
            toResult.add(getTransactionAsStringList(t));
        }
        AjaxTransaction at = new AjaxTransaction(input);
        return at;
    }
/*
* prawdopodonbnie nie potrzebna metoda */
    private List<String>  getTransactionAsStringList(Transaction t) {
        List<String> result = new ArrayList<>();
        result.add(t.getCourses().getCurrency().getIdCode());
        result.add(t.getQty().toString());
        result.add(t.getRatio().toString());
        result.add(t.getValue().toString());
        result.add(t.getTransactionDate().format(DateTimeFormatter.ISO_DATE_TIME));
        return result;
    }
}
