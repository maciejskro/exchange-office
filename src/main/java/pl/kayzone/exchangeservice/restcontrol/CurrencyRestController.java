package pl.kayzone.exchangeservice.restcontrol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kayzone.exchangeservice.entity.Currency;
import pl.kayzone.exchangeservice.entity.NbpCurrencyTable;
import pl.kayzone.exchangeservice.entity.dto.AjaxTransaction;
import pl.kayzone.exchangeservice.repository.NbpApiRepository;
import pl.kayzone.exchangeservice.services.CurrencyServices;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(value = "/v1")
public class CurrencyRestController {

    @Autowired
    private CurrencyServices currencyServices;


    @PostMapping("/currency/save")
    public ResponseEntity<Void> saveCurrency(@RequestBody List<Currency> c) {
        //TODO : extract Currency to dto
        for (Currency curr: c) {
            curr.setLastUpdate(LocalDateTime.now());
            currencyServices.saveCurr(curr);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/currency/available")
    public  ResponseEntity<List<Currency>> getAvailableCurrency() {
        return ResponseEntity.ok(currencyServices.getAllActualCurrency());
    }

    @GetMapping("/currency/NBPmidtable")
    public ResponseEntity<List<NbpCurrencyTable>> getNbpAvailableCurrency() {
        return ResponseEntity.ok(currencyServices.getNbpCurrencyMidTable());
    }
}

