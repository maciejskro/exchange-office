package pl.kayzone.exchangeservice.restcontrol;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kayzone.exchangeservice.entity.Transaction;
import pl.kayzone.exchangeservice.entity.dto.AjaxTransaction;
import pl.kayzone.exchangeservice.services.TransactionService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.function.Supplier;

@RestController
public class TransactionRestControl {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private Supplier<LocalDateTime> localDateTimeSupplier;

    @GetMapping("v1/trans/showTransactions")
    public ResponseEntity<AjaxTransaction> getAllAcutalCurrency() {
        LocalDateTime today = localDateTimeSupplier.get();
        try {
             return  ResponseEntity.ok(transactionService.findByTransactionDateBetween ( today ));
        } catch (IndexOutOfBoundsException e) {
             return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("v1/trans/sell")
    public void sellCurrency(Transaction t) {

        transactionService.sell(t);
    }

    @PostMapping("v1/trans/buy")
    public void buyCurrency(Transaction t) {
        transactionService.buy(t);
    }

}
