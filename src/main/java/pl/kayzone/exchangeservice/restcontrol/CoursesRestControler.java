package pl.kayzone.exchangeservice.restcontrol;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.kayzone.exchangeservice.entity.CurrencyCourses;
import pl.kayzone.exchangeservice.services.CurrencyCourseServices;

@RestController
public class CoursesRestControler {

    private CurrencyCourseServices ccs;


    @Autowired
    public void setCcs(CurrencyCourseServices ccs) {
        this.ccs = ccs;
    }

    @GetMapping("/v1/courses/propositon/{idCode}")
    public Optional<CurrencyCourses> getCoursesProposition(@PathVariable String idCode) {
        Optional<CurrencyCourses> cc = null;
        cc = Optional.of(ccs.findProposition(idCode));
        return cc;
    }

    @GetMapping("/v1/courses/current/{idCode}")
    public Optional<CurrencyCourses> getCurrentCourse(@PathVariable String idCode) {
       return ccs.findAllByValidNow()
                         .stream()
                         .filter( x -> x.getCurrency().getIdCode().equals(idCode))
              .findAny();
    }

    @PostMapping("/v1/courses/save")
    public void setCourses (@RequestBody String saveCourses) {
        try {
            ccs.saveCurrencyCourse(saveCourses);
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }
    @PostMapping("/v1/courses/delete")
    public void deleteCourses(@RequestBody String deleteCourse) {
        try {

            ccs.deleteCurrencyCourse(deleteCourse);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
