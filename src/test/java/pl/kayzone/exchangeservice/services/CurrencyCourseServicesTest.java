package pl.kayzone.exchangeservice.services;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import pl.kayzone.exchangeservice.entity.Currency;
import pl.kayzone.exchangeservice.entity.CurrencyCourses;
import pl.kayzone.exchangeservice.entity.OfficeLocation;
import pl.kayzone.exchangeservice.entity.User;
import pl.kayzone.exchangeservice.repository.CurrencyCourseRepository;
import pl.kayzone.exchangeservice.repository.NbpApiRepository;

import static org.mockito.Mockito.*;

public class CurrencyCourseServicesTest
{
   @Mock Logger LOGGER;
   @Mock CurrencyCourseRepository currCourRepo;
   @Mock CurrencyServices cs;
   @Mock NbpApiRepository nbpRepo;
   @Mock HashMap<String, CurrencyCourses> cachedList;
   @InjectMocks CurrencyCourseServices currencyCourseServices;

   @Before public void setUp()
   {
      MockitoAnnotations.initMocks(this);
   }

   @Test public void testFindAllByValidNow() throws Exception
   {
      when(currCourRepo.findAllByValidFromBeforeAndValidToAfter(any(), any())).thenReturn(
            Arrays.<CurrencyCourses>asList(new CurrencyCourses(
                  new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                        LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)),
                  new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(
                        new User("username", "fistname", "lastname", "userRole", "email", "pass", null))),
                  new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))));

      List<CurrencyCourses> result = currencyCourseServices
            .findAllByValidNow(LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58));
      Assert.assertEquals(Arrays.<CurrencyCourses>asList(new CurrencyCourses(
            new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)),
            new OfficeLocation("shortName", "fullName", "address", "zipCode", "city",
                  Arrays.<User>asList(new User("username", "fistname", "lastname", "userRole", "email", "pass", null))),
            new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
            LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))), result);
   }

   @Test public void testFindAllByValidNow2() throws Exception
   {
      when(currCourRepo.findAllByValidFromBeforeAndValidToAfter(any(), any())).thenReturn(
            Arrays.<CurrencyCourses>asList(new CurrencyCourses(
                  new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                        LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)),
                  new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(
                        new User("username", "fistname", "lastname", "userRole", "email", "pass", null))),
                  new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))));

      List<CurrencyCourses> result = currencyCourseServices.findAllByValidNow();
      Assert.assertEquals(Arrays.<CurrencyCourses>asList(new CurrencyCourses(
            new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)),
            new OfficeLocation("shortName", "fullName", "address", "zipCode", "city",
                  Arrays.<User>asList(new User("username", "fistname", "lastname", "userRole", "email", "pass", null))),
            new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
            LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))), result);
   }

   @Test public void testFindProposition() throws Exception
   {
      when(cs.getAllActualCurrency()).thenReturn(Arrays.<Currency>asList(
            new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))));
      when(nbpRepo.getCurrentRate4Currency(anyString(), anyString())).thenReturn(null);

      CurrencyCourses result = currencyCourseServices.findProposition("idCode");
      Assert.assertEquals(new CurrencyCourses(
            new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)),
            new OfficeLocation("shortName", "fullName", "address", "zipCode", "city",
                  Arrays.<User>asList(new User("username", "fistname", "lastname", "userRole", "email", "pass", null))),
            new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
            LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)), result);
   }

   @Test public void testSaveCurrencyCourse() throws Exception
   {
      when(currCourRepo.findAllByValidFromBeforeAndValidToAfter(any(), any())).thenReturn(
            Arrays.<CurrencyCourses>asList(new CurrencyCourses(
                  new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                        LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)),
                  new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(
                        new User("username", "fistname", "lastname", "userRole", "email", "pass", null))),
                  new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))));
      when(currCourRepo.findByCurrency_IdCodeAndValidFromIsBeforeAndValidToIsAfter(anyString(), any(), any()))
            .thenReturn(Arrays.<CurrencyCourses>asList(new CurrencyCourses(
                  new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                        LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)),
                  new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(
                        new User("username", "fistname", "lastname", "userRole", "email", "pass", null))),
                  new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))));
      when(currCourRepo.findByCurrency_IdCodeAndValidFromIsAfterAndValidToIsBefore(anyString(), any(), any()))
            .thenReturn(Arrays.<CurrencyCourses>asList(new CurrencyCourses(
                  new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                        LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)),
                  new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(
                        new User("username", "fistname", "lastname", "userRole", "email", "pass", null))),
                  new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))));
      when(currCourRepo.findByCurrency_IdCodeAndValidFromIsOrValidToIs(anyString(), any(), any())).thenReturn(
            Arrays.<CurrencyCourses>asList(new CurrencyCourses(
                  new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                        LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58)),
                  new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(
                        new User("username", "fistname", "lastname", "userRole", "email", "pass", null))),
                  new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58),
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))));
      when(cs.getAllActualCurrency()).thenReturn(Arrays.<Currency>asList(
            new Currency("idCode", "name", "urlNbp", "tableTypes", new BigDecimal(0), Boolean.TRUE,
                  LocalDateTime.of(2019, Month.AUGUST, 8, 17, 39, 58))));

      currencyCourseServices.saveCurrencyCourse("strtosave");
   }
}
