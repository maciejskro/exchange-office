package pl.kayzone.exchangeservice.services;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.kayzone.exchangeservice.entity.OfficeLocation;
import pl.kayzone.exchangeservice.entity.User;
import pl.kayzone.exchangeservice.entity.wrappers.UserPrincipalDetail;

import static org.mockito.Mockito.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;

public class UserSecurityServiceTest {
    @Mock
    MongoTemplate mongoTemplate;

    @InjectMocks
    UserSecurityService userSecurityService;

    private OfficeLocation officeLocation;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testLoadUserByUsername() throws Exception {
        OfficeLocation offLoc = new OfficeLocation();
            offLoc.setShortName("koscilkowa");
            offLoc.setAddress("address");
            offLoc.setCity("krakow");
            offLoc.setFullName("kosciółkowa");
        User user = new User("username", "fistname", "lastname", "userRole", "email", "pass",offLoc);
        Query q = new Query(where("username").is("username"));
        Mockito.when(mongoTemplate.findOne(q,User.class)).thenReturn(user);

        UserDetails result = userSecurityService.loadUserByUsername("username");

        Assert.assertEquals(new UserPrincipalDetail(user), result);
        Mockito.verify(mongoTemplate,times(1)).findOne(q,User.class);
    }

    @Test
    public void shouldThrowExceptionWhenWrongUser() {
        User user = new User("username", "fistname", "lastname", "userRole", "email", "pass", new OfficeLocation());
        Query q = new Query(where("username").is("username"));
        Mockito.when(mongoTemplate.findOne(q,User.class)).thenReturn(user);

        Assertions.assertThatThrownBy( () -> {userSecurityService.loadUserByUsername("innyuser"); })
            .isInstanceOf(UsernameNotFoundException.class)
            .hasMessage("innyuser");
    }
}

