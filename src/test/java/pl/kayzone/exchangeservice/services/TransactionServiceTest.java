package pl.kayzone.exchangeservice.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.kayzone.exchangeservice.entity.*;
import pl.kayzone.exchangeservice.entity.dto.AjaxTransaction;
import pl.kayzone.exchangeservice.repository.TransactionRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class TransactionServiceTest {
    @Mock
    TransactionRepository repository;
    @InjectMocks
    TransactionService transactionService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindByTransactionDateBetween() throws Exception {

        when(repository.findByTransactionDateBetween(any(), any())).thenReturn(Arrays.<Transaction>asList(new Transaction(new CurrencyCourses(new Currency("idCode", "name", "urlNbp", "tableTypes", BigDecimal.valueOf(0),true, LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)), new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(new User("username", "fistname", "lastname", "userRole", "email", "pass", null))), new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51))));

        AjaxTransaction result = transactionService.findByTransactionDateBetween(LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51));
        Assert.assertEquals(Arrays.<Transaction>asList(new Transaction(new CurrencyCourses(new Currency("idCode", "name", "urlNbp", "tableTypes", BigDecimal.valueOf(0),true, LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)), new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(new User("username", "fistname", "lastname", "userRole", "email", "pass", null))), new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51))), result);
    }

    @Test
    public void testSell() throws Exception {
        transactionService.sell(new Transaction(new CurrencyCourses(new Currency("idCode", "name", "urlNbp", "tableTypes", BigDecimal.valueOf(0),true, LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)), new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(new User("username", "fistname", "lastname", "userRole", "email", "pass", null))), new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)));
    }

    @Test
    public void testBuy() throws Exception {
        transactionService.buy(new Transaction(new CurrencyCourses(new Currency("idCode", "name", "urlNbp", "tableTypes", BigDecimal.valueOf(0),true, LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)), new OfficeLocation("shortName", "fullName", "address", "zipCode", "city", Arrays.<User>asList(new User("username", "fistname", "lastname", "userRole", "email", "pass", null))), new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), LocalDateTime.of(2018, Month.JULY, 1, 12, 35, 51)));
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme