package pl.kayzone.exchangeservice.restcontrol;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pl.kayzone.exchangeservice.entity.*;
import pl.kayzone.exchangeservice.entity.dto.AjaxTransaction;
import pl.kayzone.exchangeservice.repository.TransactionRepository;
import pl.kayzone.exchangeservice.services.TransactionService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import static org.mockito.Mockito.*;

public class TransactionRestControlTest {

    @Mock
    TransactionService transactionService;

    @Mock
    Supplier<LocalDateTime> localDateTimeSupplier;
    @InjectMocks
    TransactionRestControl transactionRestControl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllActualCurrency() throws Exception {

        LocalDateTime testDate  = LocalDateTime.of(2018, Month.JUNE, 29, 01, 00);
        Transaction testTrans = new Transaction(
                new CurrencyCourses(
                        new Currency("idCode", "name", "urlNbp", "tableTypes",
                                BigDecimal.valueOf(0), true, testDate),
                        new OfficeLocation("shortName", "fullName", "address", "zipCode", "city"
                                , Arrays.<User>asList(
                                new User("username", "fistname", "lastname", "userRole", "email", "pass", null)
                        )),
                        new BigDecimal(0), new BigDecimal(0),
                        testDate,
                        testDate),
                new BigDecimal(0), new BigDecimal(0), new BigDecimal(0),
                testDate);


        when(localDateTimeSupplier.get()).thenReturn(testDate);
        when(transactionService.findByTransactionDateBetween(testDate))
                .thenReturn(new AjaxTransaction(Arrays.asList(testTrans) ) );


        ResponseEntity<AjaxTransaction> result = transactionRestControl.getAllAcutalCurrency();


        Assertions.assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(result).isInstanceOf(ResponseEntity.class);
        Mockito.verify(transactionService).findByTransactionDateBetween(testDate);
    }

}
