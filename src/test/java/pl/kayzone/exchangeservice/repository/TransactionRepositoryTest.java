package pl.kayzone.exchangeservice.repository;


import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;
import pl.kayzone.exchangeservice.entity.CurrencyCourses;
import pl.kayzone.exchangeservice.entity.Transaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TransactionRepositoryTest {

    @Autowired
    private TransactionRepository tr;
    @Autowired
    private CurrencyCourseRepository ccr;

    @Test
    public void shouldSaveTransaction(){
        Transaction t = new Transaction();

        CurrencyCourses cc = null;
         if ( ! CollectionUtils.isEmpty(ccr.findAll()) ) {
            cc = ccr.findAll().get(1);
         }
            t.setCourses(cc);
            t.setQty(new BigDecimal(100.0));
            t.setRatio((cc.getAsk()));
            t.setTransactionDate(LocalDateTime.now());
            t.setValue(t.getQty().multiply(t.getRatio()));
            Transaction transResult = tr.save(t);

        Assertions.assertThat(transResult).isEqualTo(t);
    }

}
