package pl.kayzone.exchangeservice.repository;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;
import pl.kayzone.exchangeservice.entity.Currency;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CurrencyRepositoryTest {

    @Autowired
    private CurrencyRepository cr;

    private static Currency currency;

    @BeforeClass
    public static void setUp() {
            currency = new Currency("USD","dolar",
                    "http://api.nbp.pl/api/exchangerates/rates/{table}/{code}/",
                    "A",BigDecimal.valueOf(1.0),true, LocalDateTime.now().minusDays(1l));
    }

    @Test
    public void testSavedSingleCurrency() {
        if (CollectionUtils.isEmpty(cr.findAll())) {
            cr.save(currency);
        }

        Assertions.assertThat(cr.findAll()).hasOnlyElementsOfType(Currency.class);
        Assertions.assertThat(cr.findById("USD")).isNotEmpty();

    }

    @Test
    public void testSavedEuro() {
        Currency c = new Currency("EUR","euro",
                "http://api.nbp.pl/api/exchangerates/rates/{table}/{code}/",
                "A",BigDecimal.valueOf(1.0),true, LocalDateTime.now().minusDays(1l));

        cr.save(c);

        Assertions.assertThat(cr.findById("EUR")).hasValue(c);
    }
}
