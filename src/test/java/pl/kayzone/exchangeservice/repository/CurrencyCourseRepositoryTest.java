package pl.kayzone.exchangeservice.repository;


import org.assertj.core.api.Assertions;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;
import pl.kayzone.exchangeservice.entity.Currency;
import pl.kayzone.exchangeservice.entity.CurrencyCourses;
import pl.kayzone.exchangeservice.entity.OfficeLocation;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CurrencyCourseRepositoryTest {

    private static CurrencyCourses cc;

    @Autowired
    private CurrencyCourseRepository ccr;
    @Autowired
    private CurrencyRepository cr;

    @BeforeClass
    public static void setUpClass() {
        cc = new CurrencyCourses();
        cc.setCurrency(new Currency("EUR","euro","http://api.nbp.pl/api/exchangerates/rates/{table}/{code}/",
                            "A",BigDecimal.valueOf(1.0),true, LocalDateTime.now().minusDays(1l)));
        cc.setValidFrom(LocalDateTime.now().minusDays(2l));
        cc.setValidTo(LocalDateTime.now().plusDays(1l));
        cc.setAsk(new BigDecimal(3.44));
        cc.setBid(new BigDecimal(3.56));
        cc.setOfficeLocation(new OfficeLocation("nazwa","fulname","addres","32-080","zbierzów",null));
    }


    @Test
    public void testSavedSingleCourses() {
        List<CurrencyCourses> result = ccr.findAll();
        cr.findById("EUR").ifPresent(currency -> cc.setCurrency(currency));
        System.out.println(cc.toString());
       // if (CollectionUtils.isEmpty(result)) {
            ccr.save(cc);
        //}

        List<CurrencyCourses> lista = ccr.findAll();

        Assertions.assertThat(lista).hasOnlyElementsOfType(CurrencyCourses.class);
        System.out.println(lista.get(0).toString());
    }

    @After
    public void cleanUP() {

    }
}