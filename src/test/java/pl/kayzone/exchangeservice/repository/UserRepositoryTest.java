package pl.kayzone.exchangeservice.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.kayzone.exchangeservice.entity.User;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void shouldReturnNonNullUsers() {

        List<User> testList = userRepository.findAll();

        Assertions.assertThat(testList).hasOnlyElementsOfType(User.class);
        Assertions.assertThat(testList.size()).isGreaterThanOrEqualTo(1);
    }
}
