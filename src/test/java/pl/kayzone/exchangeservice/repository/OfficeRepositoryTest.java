package pl.kayzone.exchangeservice.repository;


import org.assertj.core.api.Assertions;
import org.bson.types.ObjectId;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;
import pl.kayzone.exchangeservice.entity.Office;
import pl.kayzone.exchangeservice.entity.OfficeLocation;
import pl.kayzone.exchangeservice.entity.User;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class OfficeRepositoryTest {

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private UserRepository userRepository;


    private static Office office;
    private static List<User> userList;

    @BeforeClass
    public static void setUp() {
        userList = new ArrayList<>();
            userList.add(new User("admin", "admin", "admin",
                    "ADMIN","admin@gmail.pl","admin123",null));
            userList.add(new User("user", "user", "user",
                    "USER","user@gmail.com","user123",null));
       List<OfficeLocation> officeList = new ArrayList<>();
            officeList.add(new OfficeLocation("marynarska", "Kantor Józia",
                    "Marnarska 10", "32-100", "zabierzów",userList));
            officeList.add(new OfficeLocation("rynek", "u Zosi",
                    "Rynek10", "22-322", "kraków", userList));
        office = new Office(null, "superKantor", "23312", officeList);

    }

    @Test
    public void shouldSaveSingleOffice() {
       // if (CollectionUtils.isEmpty(userRepository.findAll())) {
            List<User> lu = userRepository.saveAll(userList);
       // }
        Office o = null;
        System.out.println(office.toString());
            if (CollectionUtils.isEmpty(officeRepository.findAll())) {
            o = officeRepository.save(office);
        }


        List<Office> testList = officeRepository.findAll();

        Assertions.assertThat(testList).hasOnlyElementsOfType(Office.class);
        Assertions.assertThat(o).isEqualTo(office);

    }
}
